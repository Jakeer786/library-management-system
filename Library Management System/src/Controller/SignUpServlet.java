
package Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.*;

import Bean.SignUpBean;
import DAO.SignUpDao;
@WebServlet( urlPatterns = "/CreateServlet", name = "SignUpServlet")

public class SignUpServlet extends HttpServlet {
	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 
 
 String username = request.getParameter("Username");
 String Password = request.getParameter("Password");
 String ConfirmPassword = request.getParameter("ConfirmPassword");
 String Address = request.getParameter("Address");
 String Phonenumber = request.getParameter("Phonenumber");
 String Emailid = request.getParameter("Emailid");
 
 
SignUpBean loginBean = new SignUpBean();
 

loginBean.setUsername(username); 
loginBean.setPassword(Password);
loginBean.setConfirmPassword(ConfirmPassword);
loginBean.Address(Address);
loginBean.setPhonenumber(Phonenumber);
loginBean.setEmailid(Emailid);

 
SignUpDao loginDao = new SignUpDao();
 
int userValidate = loginDao.save(loginBean); 

if(userValidate !=0) 
 {
 request.setAttribute("Username", username); 
 request.getRequestDispatcher("/SignUp.html").forward(request, response);
 }
 else
 {
 request.setAttribute("errMessage", userValidate);
 request.getRequestDispatcher("/index.html").forward(request, response);
 }
 }
}
