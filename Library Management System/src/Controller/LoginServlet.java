package Controller;

 import java.io.IOException;
 import javax.servlet.ServletException;
 import javax.servlet.http.HttpServlet;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletResponse;
 
 import Bean.LoginBean;
 import DAO.LoginDao;
 
public class LoginServlet extends HttpServlet {
 
public LoginServlet() {
 }
 
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 
 
 String Username = request.getParameter("Username");
 String Password = request.getParameter("Password");
 System.out.println(Username);
 
LoginBean loginBean = new LoginBean();
 
loginBean.setUsername(Username); 
loginBean.setPassword(Password);
 
LoginDao loginDao = new LoginDao();

String userValidate = loginDao.authenticateUser(loginBean); 

if(userValidate.equals("SUCCESS")) 
 {
	 System.out.println("successfully");
 request.setAttribute("Username", Username); 
 request.getRequestDispatcher("/Login.html").forward(request, response);
 }
 else
 {
 request.setAttribute("errMessage", userValidate);
 request.getRequestDispatcher("/index.html").forward(request, response);
 }
 }

}
 

