package Bean;

public class SignUpBean
 {
 private String Username;
 private String Password;
 private String Emailid;
 private String Phonenumber;
 private String ConfirmPassword;
 private String Address;

 public String getUsername() {
 return Username;
 }
 public void setUsername(String Username) {
 this.Username = Username;
 }
 public String getPassword() {
 return Password;
 }
 public void setPassword(String Password) {
 this.Password = Password;
 }

 public String getConfirmPassword() {
     return ConfirmPassword;
	}
 public void setConfirmPassword(String ConfirmPassword) {
	 this.ConfirmPassword = ConfirmPassword;
	 }
 
 public String getEmailid() {
	 return Emailid;
	 }
 public void setEmailid(String Emailid) {
	 this.Emailid = Emailid;
	 }
 public String getPhonenumber() {
	return Phonenumber;
	}
 public void setPhonenumber(String  Phonenumber) {
	 this.Phonenumber = Phonenumber;
	 }
 public String getAddress() {
      return Address;
     }
 public void Address(String Address) {
	 this.Address= Address;
	 }


}