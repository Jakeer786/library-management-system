 package DAO;
 
 import Bean.SignUpBean;
 import java.sql.*;
 import java.util.ArrayList;
 import java.util.List;

public class SignUpDao {
public static Connection getConnection(){
	Connection con=null;
	try{
		Class.forName("com.mysql.cj.jdbc.Driver");
		con=DriverManager.getConnection("jdbc:mysql://localhost:3306/jack","root","1234");
	}catch(Exception e){System.out.println(e);}
	return con;
}
public int save(SignUpBean u){
	int status = 0;
	try{
            
		Connection con=getConnection();
		PreparedStatement ps=con.prepareStatement("insert into Users(Username, Password, ConfirmPassword, Address, Phonenumber, Emailid) values(?,?,?,?,?,?)");
		
        ps.setString(1,u.getUsername());
		ps.setString(2,u.getPassword());
		ps.setString(3,u.getConfirmPassword());
		ps.setString(4,u.getAddress());
		ps.setString(5,u.getPhonenumber());
		ps.setString(6,u.getEmailid());
		
		status=ps.executeUpdate();
                
	}catch(Exception e){System.out.println(e);}
	return status;
}
}