
package DAO;

import java.sql.Connection;
 import java.sql.ResultSet;
 import java.sql.SQLException;
 import java.sql.Statement;
 import Bean.LoginBean;
import Connect.DBConnection;

 public class LoginDao {
 public String authenticateUser(LoginBean loginBean)
 {
 
String Username = loginBean.getUsername(); 
 String Password = loginBean.getPassword();
 
Connection con = null;
 Statement statement = null;
 ResultSet resultSet = null;
 
String UsernameDB = "";
 String PasswordDB = "";
 
try
 {
 con = DBConnection.createConnection(); 
 statement = con.createStatement(); 
 resultSet = statement.executeQuery("select Username,Password from Users");
 
while(resultSet.next()) 
 {
  UsernameDB = resultSet.getString("Username"); 
  PasswordDB = resultSet.getString("Password");
 
   if(Username.equals(UsernameDB) && Password.equals(PasswordDB))
   {
      return "SUCCESS"; 
   }
 }
 }
 catch(SQLException e)
 {
 e.printStackTrace();
 }
 return "Invalid user credentials"; 
 }
 }